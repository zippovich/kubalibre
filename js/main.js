$(function() {
    Modernizr.load([{
        // Text Shadows
        test: Modernizr.textshadow,
        nope: ['js/jquery.textshadow.js', 'css/jquery.textshadow.css'],
        complete: function() {
            if (!Modernizr.textshadow) {
                $('.tp-left ul li a').textshadow('-1px -1px 1px #2a3a07').hover(function() {
                    $(this).textshadow('1px 1px 1px #2a3a07');
                }, function() {
                    $(this).textshadow('-1px -1px 1px #2a3a07');
                });
                $('.download-button').textshadow('1px 1px 2px #376283').hover(function() {
                    $(this).textshadow('3px 3px 2px #376283');
                }, function() {
                    $(this).textshadow('1px 1px 2px #376283');
                });
            }
        }
    }]);
    
    // Multiple Backgrounds
    if (!Modernizr.multiplebgs) {
        $('.main-image').append('<div class="mi-1"><div class="mi-2"></div></div>');
    }
    
    // Border Radius
    if (!Modernizr.borderradius) {
        $('.tp-left ul li a, header > nav ul li a, .paginator ul li a, .jspTrack, .note-button').css({
            'behavior': 'url(js/PIE.htc)',
            'position': 'relative'
        });
    }
    
    // Box Shadow
    if (!Modernizr.boxshadow) {
        $('.tlw-image-column a').css({
            'behavior': 'url(js/PIE.htc)',
            'position': 'relative'
        });
    }
    
    // Paginator
    $('.paginator ul').each(function(index, item) {
        var pages = $('li', $(item));
        $(item).css({
            'width': 38 * pages.length,
            'display': 'block'
        });
    });
    $('.pag-wrapper').jScrollPane({
        horizontalDragMinWidth: 48,
        horizontalDragMaxWidth: 48
    });
    
    // Torrents List css classes
    var listItems = $('.torrent-list > li');
    listItems.each(function(index, item) {
        var addClass = (index%2)?'even':'odd';
        $(item).addClass(addClass);
        if (index<2) $(item).addClass('firstLine');
        if (index>listItems.length-3) $(item).addClass('lastLine');
        if (index%2) {
            if ($(listItems[index-1]).height() < $(listItems[index]).height()) $(listItems[index-1]).height($(listItems[index]).height());
            else $(listItems[index]).height($(listItems[index-1]).height());
        }
    });
    
    // Selects
    var params = {
        changedEl: ".sf-fields select",
        visRows: 10
    }
    cuSel(params);
    
    // tags menu
    $('.tags li').hover(function() {
        if (!$(this).hasClass('active')) {
            $('span', $(this)).stop().animate({
                'bottom': -22,
                'height': 22
            }, 150);
        }
    }, function() {
        if (!$(this).hasClass('active')) {
            $('span', $(this)).stop().css({
                'bottom': 0,
                'height': 0
            });
        }
    });
});
